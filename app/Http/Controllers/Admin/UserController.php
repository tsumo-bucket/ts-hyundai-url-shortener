<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserPasswordUpdateRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\UserRole;
use Hash;
use SumoMail;

use App\Acme\Facades\Activity;
use App\Acme\Facades\General;
use App\Acme\Facades\Option;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware('super', ['only' => ['edit', 'update']]);
    }

    public function index(Request $request)
    {
        if ($name = $request->name) {
            $users = User::where('name', 'LIKE', '%' . $name . '%')->paginate(25);
        } else {
            $users = User::paginate(25);
        }
        $pagination = $users->appends($request->except('page'))->links();

        return view('admin/users/index')
            ->with('title', 'Users')
            ->with('menu', 'users')
            ->with('data', $users)
            ->with('pagination', $pagination);
    }

    public function datatable()
    {
        return view('admin/users/datatable')->with('data', User::all());
    }

    public function create()
    {
        Option::email();
        $user_roles = UserRole::pluck('name','id');
        return view('admin/users/create')
            ->with('user_roles',$user_roles)
            ->with('title', 'Create User');
    }
    
    public function store(UserStoreRequest $request)
    {
        $input = $request->all();
        $options = Option::email();

        $data = [];
        $data['user'] = $input;
        $data['password'] = str_random(8);
        $data['options'] = $options;
      
        $params=[];
        $params['from_email'] = $options['email'];
        $params['from_name'] =$options['name'];
        $params['subject'] = $options['name'] . ': User account created';
        $params['to'] = $input['email'];

        //not required fields
        $params['replyTo'] =  $options['email'];

        //DEBUG Options are as follows:
        // FALSE - Default
        // TRUE - Shows HTML output upon sending
        // SIMULATE = Does not send email but saves information to the database
        $params['debug'] = false; 
            
        $return= SumoMail::send('emails.user-create', $data, $params);

        
        
        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->cms = $input['cms'];
        $user->password = bcrypt($data['password']);
        $user->verified = 1;
        $user->status = 'active';
        $user->type = $input['type'];
        $user->user_role_id = $input['user_role_id'];
        $user->save();

        $log = 'creates a new user "' . $user->name . '"';
        Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to users list.',
            'resetForm'=>true,
            'redirect'=>route('adminUsers')
        ];

        return response()->json($response);
    }

    public function show($id)
    {
        return view('admin/users/show')
            ->with('title', 'Show User')
            ->with('user', User::findOrFail($id));
    }

    public function edit($id)
    {
        $user_roles = UserRole::pluck('name','id');
        return view('admin/users/edit')
            ->with('title', 'Edit User')
            ->with('menu', 'users')
            ->with('user_roles',$user_roles)
            ->with('user', User::findOrFail($id));
    }

    public function change_password($id){
        $user_roles = UserRole::pluck('name','id');
        
        if(Auth::user()->type != 'super'){
            abort(403, "Unauthorized action.");
        }
        return view('admin/users/password_change')
            ->with('title', 'Change Password')
            ->with('menu', 'users')
            ->with('user_roles',$user_roles)
            ->with('user', User::findOrFail($id));
    }

    public function user_password_update(UserPasswordUpdateRequest $request, $id){
        $input = $request->all();
        $isChecked = $request->has('send_email');

        $logged_user = User::findOrFail(Auth::user()->id);

        if ($input['new'] == $input['new_confirmation']){
            $user = User::findOrFail($id);
            $user->password = Hash::make($input['new']);
            $user->save();

            $log = $logged_user['name'] . ' updates password of ' . $user['name'];
            Activity::create($log);

            if ($isChecked){
                $options = Option::email();
                $data = [];
                $data['user'] = $user;
                $data['password'] = $input['new'];
                $data['options'] = $options;
                $data['logged'] = $logged_user;
            
                $params=[];
                $params['from_email'] = $options['sender-email'];
                $params['from_name'] =$options['name'];
                $params['subject'] = $options['name'] . ': Password change';
                $params['to'] = $user['email'];

                //not required fields
                $params['replyTo'] =  $options['email'];

                //DEBUG Options are as follows:
                // FALSE - Default
                // TRUE - Shows HTML output upon sending
                // SIMULATE = Does not send email but saves information to the database
                $params['debug'] = false;
                    
                $return = SumoMail::send('emails.password-change', $data, $params);
            }

            $response = [
                'notifTitle'=>'Save successful.',
                'notifMessage'=>'Redirecting to users list.',
                'resetForm'=>true,
                'redirect'=>route('adminUsers')
            ];

        } else{
            $response = [
                'notifTitle'=>'Password does not match.',
            ];
        }

        return response()->json($response);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $input = $request->all();

        $user = User::findOrFail($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->cms = $input['cms'];
        $user->type = $input['type'];
        $user->user_role_id = $input['user_role_id'];
        $user->save();

        $log = 'edits a user "' . $user->name . '"';
        Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function destroy(Request $request)
    {
        $input = $request->all();

        $samples = User::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($samples as $s) {
            $names[] = $s->name;
        }
        $log = 'deletes a new sample "' . implode(', ', $names) . '"';
        Activity::create($log);

        User::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminUsers')
        ];

        return response()->json($response);
    }
}
