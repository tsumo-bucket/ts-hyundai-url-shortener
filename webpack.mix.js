const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// create global css imports
// create admin css imports
// this gets imported in app and admin
mix.sass('resources/sass/banner-video-modal.scss', 'public/css');
// mix.sass('resources/sass/admin-import.scss', 'public/css');
// mix.sass('resources/sass/admin.scss', 'public/css');
mix.sass('resources/sass/app-import.scss', 'public/css');
mix.sass('resources/sass/app.scss', 'public/css');

// ALL.JS -- global scripts`
const all = [
  // LIBRARIES FROM NODE MODULES
  'node_modules/jquery/dist/jquery.min.js',
  // 'node_modules/parselyjs/dist/parsley.min.js',
  'node_modules/popper.js/dist/umd/popper.min.js',
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/handlebars/dist/handlebars.min.js',
  'node_modules/underscore/underscore-min.js',

  // LIBRARIES FROM RESOURCES
  'resources/assets/js/sumo-plugins/sumo-app-asset-helper.js',
  'resources/assets/js/unload-warning.min.js',
  'resources/assets/js/sweetalert.min.js',
  ];

mix.scripts(all, 'public/js/all.js');


// APP.JS -- front end scripts
mix.scripts([
  'resources/assets/js/app.js'
  ], 'public/js/app.js');


// ADMIN.JS -- backend scripts
mix.scripts([... all,
  // LIBRARIES FROM NODE MODULES
  'node_modules/select2/dist/js/select2.full.min.js',

  // LIBRARIES FROM RESOURCES
  'resources/assets/js/jquery.dataTables.min.js',
  'resources/assets/js/jquery.sumo-datepicker.min.js',
  'resources/assets/js/dataTables.bootstrap.min.js',
  'resources/assets/js/parsley.min.js',
  'resources/assets/js/jquery-ui.min.js',
  'resources/assets/js/bootstrap-notify.min.js',
  'resources/assets/js/Jcrop.min.js',
  'resources/assets/js/handlebars.js',
  'resources/assets/js/redactor.min.js',
  'resources/assets/js/bootstrap-toggle.min.js',
  'resources/assets/js/unload-warning.js',
  'resources/assets/js/sweetalert.min.js',
  'resources/assets/js/jquery.fancybox.min.js',
  'resources/assets/js/sumo-plugins/sumo-dropdown.js',
  'resources/assets/js/sumo-plugins/sumo-browse.js',
  'resources/assets/js/sumo-plugins/universal-sumo-browse.js',
  'resources/assets/js/sumo-plugins/sumo-number-formatter.js',
  'resources/assets/js/sumo-plugins/sumo-product-browse.js',
  'resources/assets/js/sumo-plugins/sumo-asset-manager.js',
  'resources/assets/js/sumo-plugins/sumo-video-banner.js',
  ], 'public/js/admin.js');
  
if (mix.inProduction()) {
  mix.version();
}

// CABOODLE.CSS
mix.sass('resources/sass/caboodle.scss', 'public/css/caboodle.css');

// CABOODLE.JS -- backend scripts
mix.scripts([
  'resources/assets/js/jquery.min.js',
  'resources/assets/js/bootstrap/bootstrap.bundle.min.js',
  'resources/assets/js/material/material-components-web.min.js',
  'resources/assets/js/moment-with-locales.min.js',
  'resources/assets/js/bootstrap-datetimepicker.js',
  'resources/assets/js/daterangepicker.js',
  'resources/assets/js/underscore-min.js',
  'resources/assets/js/jquery.scrollbar.min.js',
  'resources/assets/js/select2.min.js',
  'resources/assets/js/selectize.js',
  'resources/assets/js/toastr.min.js',
  'resources/assets/js/mutation-observer.js',
  'resources/assets/js/chartist.min.js',
  'resources/assets/js/Chart.min.js',
  'resources/assets/js/chart-loader.js',
  'resources/assets/js/caboodle/caboodle-global.js',
  'resources/assets/js/caboodle/caboodle-table.js',
  'resources/assets/js/caboodle/caboodle-forms.js',
  'resources/assets/js/sumo-admin.js',
], 'public/js/caboodle.js');

mix.browserSync(process.env.APP_URL);
