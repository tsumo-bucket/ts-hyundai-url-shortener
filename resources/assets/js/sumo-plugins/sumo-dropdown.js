 /*!
  * Sumo Dropdown Generator for Ecommerce Base
  * http://sumofy.me/
  *
  * Copyright Think Sumo Creative Media Inc.
  * Developed by Dona Roxas
  * Released under the MIT license.
  * http://sumofy.me/
  *
  */

(function($) {
    $.fn.sumodropdown = function(options) {
        if(typeof _.each === 'undefined') {
            throw new Error('sumo-dropdown requires Underscore.js to be loaded first');
        }

        var defaults = {
            freetext: false
        }

        //Extend those options
        var options = $.extend(defaults, options);

        $(this).each(function(e) {
            var select = $(this);
            var selectOptions = select.find('option');
            if(select[0].disabled) {
                select.wrap('<div class="sumo-dropdown dropdown-toggle"></div>');
            }
            else {
                select.wrap('<div class="sumo-dropdown dropdown-toggle" data-toggle="dropdown"></div>');
            }
            select.after('<div class="dropdown dropdown-menu"></div>');

            var dropdownParent = select.closest('sumo-dropdown');            
            var dropdown = select.next('.dropdown');

            if(options.freetext) {
                select.after('<input type="text" value="" />');
            }
            else {
                select.after('<input type="text" value="" readonly class="readonly" />');
            }

            _.each(selectOptions, function(option) {
                updateData(option);
                if(option.selected) {
                    select.siblings('input').val(option.text)
                }
            });

            select.siblings('.dropdown').children('.item').on('click', function(e) {
                setInputValue($(this));
            });

            select.siblings('input').keyup(function(e) {
                var expr = '/' + $(this).val() + '/';
                _.each(selectOptions, function(option) {
                    updateData(option);
                });
            });

            // update dropdown values
            function updateData(option) {
                $('<div class="item" data-text="' + option.text + '" data-value="' + option.value + '">' + option.text + '</div>').appendTo(dropdown);
            };

            // update input value
            function setInputValue(value) {
                select.siblings('input').val("");
                select.siblings('input').val(value.data('text'));
                select.val(value.data('value'));
                
                if (options.onChange !== undefined) {
                    options.onChange({
                        text: value.data('text'),
                        value: value.data('value'),
                    });
                }
            };
        });

    }

})(jQuery);