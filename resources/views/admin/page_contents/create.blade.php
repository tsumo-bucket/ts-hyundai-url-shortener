@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li  class="breadcrumb-item far">
      <a href="{{route('adminDashboard')}}">Dashboard</a>
    </li>
    <li class="breadcrumb-item far">
      <span>Pages</span>
    </li>
    <li class="breadcrumb-item far">
      <a href="{{route('adminClientPage', $page->slug)}}">{{ $page->name }}</a>
    </li>
    <li class="breadcrumb-item far active">
      <span>Create</span>
    </li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{route('adminClientPage', $page->slug)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop 

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{route('adminClientPage', $page->slug)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
  {!! Form::open(['route'=>'adminPageClientStore', 'class'=>'form form-parsley form-create form-clear']) !!}
    @include('admin.page_contents.form')
  {!! Form::close() !!}
@stop